<?php

declare(strict_types=1);

namespace JDecool\Clubhouse\Exception;

use JDecool\Clubhouse\ClubhouseException as LegacyException;

class ClubhouseException extends LegacyException
{
}
