<?php

declare(strict_types=1);

namespace JDecool\Clubhouse;

use RuntimeException;

/**
 * @deprecated
 */
class ClubhouseException extends RuntimeException
{
}
